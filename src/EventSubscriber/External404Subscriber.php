<?php

namespace Drupal\external_404\EventSubscriber;

use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\EventSubscriber\CustomPageExceptionHtmlSubscriber;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Subscribes to 404 and 403 exception events.
 */
class External404Subscriber extends CustomPageExceptionHtmlSubscriber {

  /**
   * Redirect to an external url on a 404 page exception.
   *
   * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent $event
   */
  private function makeRedirectionExternalPage(ExceptionEvent $event) {
    $custom_404_path = $this->configFactory->get('system.site')->get('page.404');

    if (!empty($custom_404_path) && !$this->str_starts_with($custom_404_path, '/')) {
      $redirection = new TrustedRedirectResponse($custom_404_path);

      $event->setResponse($redirection);
    }
    else {
      parent::on404($event);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function on404(ExceptionEvent $event) {
    $this->makeRedirectionExternalPage($event, '404_pages', Response::HTTP_NOT_FOUND);
  }

  /**
   * {@inheritdoc}
   */
  protected function getHandledFormats(): array {
    return ['html'];
  }

  /**
   * {@inheritdoc}
   */
  protected static function getPriority(): int {
    // Before CustomPageExceptionHtmlSubscriber.
    return parent::getPriority() + 1;
  }

  /**
   * PHP7 safe str_starts_with function().
   *
   * @param string $string
   *  Haystack to search.
   * @param string $substring
   *  Needle to look for.
   *
   * @return bool
   *  Does the Haystack start with the Needle.
   */
  protected function str_starts_with(string $string, string $substring): bool {
    // get the length of the substring
    $len = strlen($substring);

    // just return true when substring is an empty string
    if ($len == 0) {
      return true;
    }

    // return true or false based on the substring result
    return substr($string, 0, $len) === $substring;
  }

}

